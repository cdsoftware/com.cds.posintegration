package com.cds.posintegration.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MInvoice;
import org.compiere.model.MLocation;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MUser;
import org.compiere.model.X_I_Order;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
/**
 *	Import Order from I_Order
 *  @author Oscar Gomez
 * 			<li>BF [ 2936629 ] Error when creating bpartner in the importation order
 * 			<li>https://sourceforge.net/tracker/?func=detail&aid=2936629&group_id=176962&atid=879332
 * 	@author 	Jorg Janke
 * 	@version 	$Id: ImportOrder.java,v 1.2 2006/07/30 00:51:02 jjanke Exp $
 */
public class ImportOrder extends SvrProcess{

	/**	Client to be imported to		*/
	private int				m_AD_Client_ID = 0;
	/**	Organization to be imported to		*/
	private int				m_AD_Org_ID = 0;
	/**	Delete old Imported				*/
	private boolean			m_deleteOldImported = false;
	/**	Document Action					*/
	private String			m_docAction ;
	private String				m_DocumentNo;

	/** Effective						*/
	private Timestamp		m_DateValue = null;
	private Timestamp       m_DateOrdered = null;
	private int m_C_DocType_ID = 0;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("AD_Client_ID"))
				m_AD_Client_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("AD_Org_ID"))
				m_AD_Org_ID = para[i].getParameterAsInt();
			else if (name.equals("DeleteOldImported"))
				m_deleteOldImported = "Y".equals(para[i].getParameter());
			else if (name.equals("DocAction"))
				m_docAction = (String)para[i].getParameter();
			else if (name.equals("DocumentNo"))
				m_DocumentNo = (String)para[i].getParameter();
			else if (name.equals("DateOrdered"))
				m_DateOrdered = para[i].getParameterAsTimestamp();
			else if (name.equals("C_DocType_ID"))
				m_C_DocType_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		if (m_DateValue == null)
			m_DateValue = new Timestamp (System.currentTimeMillis());
		if(m_docAction==null)
			m_docAction = MOrder.DOCACTION_None;
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		StringBuilder sql = null;
		int no = 0;
		StringBuilder clientCheck = new StringBuilder(" AND AD_Client_ID=").append(m_AD_Client_ID);
		String nofiscal = "";
		String noserial = "";
		StringBuilder docnofilter = new StringBuilder("");
		StringBuilder orgfilter = new StringBuilder("");
		if(m_AD_Org_ID!=0)
			orgfilter.append(" AND o.AD_Org_ID ='").append(m_AD_Org_ID).append("'");
		
		if(m_DocumentNo!= null) {
			
			docnofilter.append(" AND o.DocumentNo ='").append(m_DocumentNo).append("'");
			docnofilter.append(" AND o.C_DocType_ID ='").append(m_C_DocType_ID).append("'");
		}
		if(m_DateOrdered!=null)
			docnofilter.append(" AND o.DateOrdered ='").append(m_DateOrdered).append("'");
		//	****	Prepare	****

		//	Delete Old Imported
		if (m_deleteOldImported)
		{
			sql = new StringBuilder ("DELETE I_Order ")
					.append("WHERE I_IsImported='Y'").append (clientCheck);
			no = DB.executeUpdate(sql.toString(), get_TrxName());
			if (log.isLoggable(Level.FINE)) log.fine("Delete Old Impored =" + no);
		}

		//	Set Client, Org, IsActive, Created/Updated
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET AD_Client_ID = COALESCE (AD_Client_ID,").append (m_AD_Client_ID).append ("),")
				.append(" AD_Org_ID = COALESCE (AD_Org_ID,").append (m_AD_Org_ID).append ("),")
				.append(" IsActive = COALESCE (IsActive, 'Y'),")
				.append(" Created = COALESCE (Created, now()),")
				.append(" CreatedBy = COALESCE (CreatedBy, 0),")
				.append(" Updated = COALESCE (Updated, now()),")
				.append(" UpdatedBy = COALESCE (UpdatedBy, 0),")
				.append(" I_ErrorMsg = ' ',")
				.append(" I_IsImported = 'N' ")
				.append("WHERE (o.I_IsImported<>'Y' OR o.I_IsImported IS NULL)");
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.INFO)) log.info ("Reset=" + no);

		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Org, '")
				.append("WHERE (AD_Org_ID IS NULL OR AD_Org_ID=0")
				.append(" OR EXISTS (SELECT * FROM AD_Org oo WHERE o.AD_Org_ID=oo.AD_Org_ID AND (oo.IsSummary='Y' OR oo.IsActive='N')))")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		/*if(m_DocumentNo!= null) {
			sql.append(orgfilter);
			sql.append(docnofilter);
		}*/
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Invalid Org=" + no);

		//	Document Type - PO - SO
		sql = new StringBuilder ("UPDATE I_Order o ")	//	PO Document Type Name
				.append("SET C_DocType_ID=(SELECT C_DocType_ID FROM C_DocType d WHERE d.Name=o.DocTypeName")
				.append(" AND d.DocBaseType='POO' AND o.AD_Client_ID=d.AD_Client_ID) ")
				.append("WHERE C_DocType_ID IS NULL AND IsSOTrx='N' AND DocTypeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
			
		}	
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set PO DocType=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")	//	SO Document Type Name
				.append("SET C_DocType_ID=(SELECT C_DocType_ID FROM C_DocType d WHERE d.Name=o.DocTypeName")
				.append(" AND d.DocBaseType='SOO' AND o.AD_Client_ID=d.AD_Client_ID) ")
				.append("WHERE C_DocType_ID IS NULL AND IsSOTrx='Y' AND DocTypeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set SO DocType=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_DocType_ID=(SELECT C_DocType_ID FROM C_DocType d WHERE d.Name=o.DocTypeName")
				.append(" AND d.DocBaseType IN ('SOO','POO') AND o.AD_Client_ID=d.AD_Client_ID) ")
				//+ "WHERE C_DocType_ID IS NULL AND IsSOTrx IS NULL AND DocTypeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
				.append("WHERE C_DocType_ID IS NULL AND DocTypeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set DocType=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")	//	Error Invalid Doc Type Name
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid DocTypeName, ' ")
				.append("WHERE C_DocType_ID IS NULL AND DocTypeName IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Invalid DocTypeName=" + no);
		//	DocType Default
		sql = new StringBuilder ("UPDATE I_Order o ")	//	Default PO
				.append("SET C_DocType_ID=(SELECT MAX(C_DocType_ID) FROM C_DocType d WHERE d.IsDefault='Y'")
				.append(" AND d.DocBaseType='POO' AND o.AD_Client_ID=d.AD_Client_ID) ")
				.append("WHERE C_DocType_ID IS NULL AND IsSOTrx='N' AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set PO Default DocType=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")	//	Default SO
				.append("SET C_DocType_ID=(SELECT MAX(C_DocType_ID) FROM C_DocType d WHERE d.IsDefault='Y'")
				.append(" AND d.DocBaseType='SOO' AND o.AD_Client_ID=d.AD_Client_ID) ")
				.append("WHERE C_DocType_ID IS NULL AND IsSOTrx='Y' AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set SO Default DocType=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_DocType_ID=(SELECT MAX(C_DocType_ID) FROM C_DocType d WHERE d.IsDefault='Y'")
				.append(" AND d.DocBaseType IN('SOO','POO') AND o.AD_Client_ID=d.AD_Client_ID) ")
				.append("WHERE C_DocType_ID IS NULL AND IsSOTrx IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Default DocType=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")	// No DocType
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No DocType, ' ")
				.append("WHERE C_DocType_ID IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No DocType=" + no);

		//	Set IsSOTrx
		sql = new StringBuilder ("UPDATE I_Order o SET IsSOTrx='Y' ")
				.append("WHERE EXISTS (SELECT * FROM C_DocType d WHERE o.C_DocType_ID=d.C_DocType_ID AND d.DocBaseType='SOO' AND o.AD_Client_ID=d.AD_Client_ID)")
				.append(" AND C_DocType_ID IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set IsSOTrx=Y=" + no);
		sql = new StringBuilder ("UPDATE I_Order o SET IsSOTrx='N' ")
				.append("WHERE EXISTS (SELECT * FROM C_DocType d WHERE o.C_DocType_ID=d.C_DocType_ID AND d.DocBaseType='POO' AND o.AD_Client_ID=d.AD_Client_ID)")
				.append(" AND C_DocType_ID IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set IsSOTrx=N=" + no);

		//	Price List
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_PriceList_ID=(SELECT MAX(M_PriceList_ID) FROM M_PriceList p WHERE p.IsDefault='Y'")
				.append(" AND p.C_Currency_ID=o.C_Currency_ID AND p.IsSOPriceList=o.IsSOTrx AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE M_PriceList_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Default Currency PriceList=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_PriceList_ID=(SELECT MAX(M_PriceList_ID) FROM M_PriceList p WHERE p.IsDefault='Y'")
				.append(" AND p.IsSOPriceList=o.IsSOTrx AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE M_PriceList_ID IS NULL AND C_Currency_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Default PriceList=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_PriceList_ID=(SELECT MAX(M_PriceList_ID) FROM M_PriceList p ")
				.append(" WHERE p.C_Currency_ID=o.C_Currency_ID AND p.IsSOPriceList=o.IsSOTrx AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE M_PriceList_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Currency PriceList=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_PriceList_ID=(SELECT MAX(M_PriceList_ID) FROM M_PriceList p ")
				.append(" WHERE p.IsSOPriceList=o.IsSOTrx AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE M_PriceList_ID IS NULL AND C_Currency_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set PriceList=" + no);
		//
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No PriceList, ' ")
				.append("WHERE o.M_PriceList_ID IS NULL")
				.append(" AND o.I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning("No PriceList=" + no);

		// @Trifon - Import Order Source
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_OrderSource_ID=(SELECT C_OrderSource_ID FROM C_OrderSource p")
				.append(" WHERE o.C_OrderSourceValue=p.Value AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE C_OrderSource_ID IS NULL AND C_OrderSourceValue IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Order Source=" + no);
		// Set proper error message
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Not Found Order Source, ' ")
				.append("WHERE C_OrderSource_ID IS NULL AND C_OrderSourceValue IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
		
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning("No OrderSource=" + no);

		//	Payment Term
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_PaymentTerm_ID=(SELECT C_PaymentTerm_ID FROM C_PaymentTerm p")
				.append(" WHERE o.PaymentTermValue=p.Value AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE C_PaymentTerm_ID IS NULL AND PaymentTermValue IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set PaymentTerm=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_PaymentTerm_ID=(SELECT MAX(C_PaymentTerm_ID) FROM C_PaymentTerm p")
				.append(" WHERE p.IsDefault='Y' AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE C_PaymentTerm_ID IS NULL AND o.PaymentTermValue IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Default PaymentTerm=" + no);
		//
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No PaymentTerm, ' ")
				.append("WHERE C_PaymentTerm_ID IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No PaymentTerm=" + no);

		//	Warehouse
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_Warehouse_ID=(SELECT MAX(M_Warehouse_ID) FROM M_Warehouse w")
				.append(" WHERE o.AD_Client_ID=w.AD_Client_ID AND o.AD_Org_ID=w.AD_Org_ID) ")
				.append("WHERE M_Warehouse_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());	//	Warehouse for Org
		if (no != 0)
			if (log.isLoggable(Level.FINE)) log.fine("Set Warehouse=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_Warehouse_ID=(SELECT M_Warehouse_ID FROM M_Warehouse w")
				.append(" WHERE o.AD_Client_ID=w.AD_Client_ID) ")
				.append("WHERE M_Warehouse_ID IS NULL")
				.append(" AND EXISTS (SELECT AD_Client_ID FROM M_Warehouse w WHERE w.AD_Client_ID=o.AD_Client_ID GROUP BY AD_Client_ID HAVING COUNT(*)=1)")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			if (log.isLoggable(Level.FINE)) log.fine("Set Only Client Warehouse=" + no);
		//
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Warehouse, ' ")
				.append("WHERE M_Warehouse_ID IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Warehouse=" + no);

		//	BP from EMail
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET (C_BPartner_ID,AD_User_ID)=(SELECT C_BPartner_ID,AD_User_ID FROM AD_User u")
				.append(" WHERE o.EMail=u.EMail AND o.AD_Client_ID=u.AD_Client_ID AND u.C_BPartner_ID IS NOT NULL) ")
				.append("WHERE C_BPartner_ID IS NULL AND EMail IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set BP from EMail=" + no);
		//	BP from ContactName
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET (C_BPartner_ID,AD_User_ID)=(SELECT C_BPartner_ID,AD_User_ID FROM AD_User u")
				.append(" WHERE o.ContactName=u.Name AND o.AD_Client_ID=u.AD_Client_ID AND u.C_BPartner_ID IS NOT NULL) ")
				.append("WHERE C_BPartner_ID IS NULL AND ContactName IS NOT NULL")
				.append(" AND EXISTS (SELECT Name FROM AD_User u WHERE o.ContactName=u.Name AND o.AD_Client_ID=u.AD_Client_ID AND u.C_BPartner_ID IS NOT NULL GROUP BY Name HAVING COUNT(*)=1)")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set BP from ContactName=" + no);
		//	BP from Value
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_BPartner_ID=(SELECT MAX(C_BPartner_ID) FROM C_BPartner bp")
				.append(" WHERE o.BPartnerValue=bp.Value AND o.AD_Client_ID=bp.AD_Client_ID) ")
				.append("WHERE C_BPartner_ID IS NULL AND BPartnerValue IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set BP from Value=" + no);
		//	Default BP
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_BPartner_ID=(SELECT C_BPartnerCashTrx_ID FROM AD_ClientInfo c")
				.append(" WHERE o.AD_Client_ID=c.AD_Client_ID) ")
				.append("WHERE C_BPartner_ID IS NULL AND BPartnerValue IS NULL AND Name IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Default BP=" + no);

		//	Existing Location ? Exact Match
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET (BillTo_ID,C_BPartner_Location_ID)=(SELECT C_BPartner_Location_ID,C_BPartner_Location_ID")
				.append(" FROM C_BPartner_Location bpl INNER JOIN C_Location l ON (bpl.C_Location_ID=l.C_Location_ID)")
				.append(" WHERE o.C_BPartner_ID=bpl.C_BPartner_ID AND bpl.AD_Client_ID=o.AD_Client_ID")
				.append(" AND TRIM(o.Address1)=TRIM(l.Address1) AND TRIM(o.Address2)=TRIM(l.Address2)")
				.append(" AND TRIM(o.City)=TRIM(l.City) AND TRIM(o.Postal)=TRIM(l.Postal)")
				.append(" AND o.C_Region_ID=l.C_Region_ID AND o.C_Country_ID=l.C_Country_ID) ")
				.append("WHERE C_BPartner_ID IS NOT NULL AND C_BPartner_Location_ID IS NULL")
				.append(" AND I_IsImported='N'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Found Location=" + no);
		//	Set Bill Location from BPartner
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET BillTo_ID=(SELECT MAX(C_BPartner_Location_ID) FROM C_BPartner_Location l")
				.append(" WHERE l.C_BPartner_ID=o.C_BPartner_ID AND o.AD_Client_ID=l.AD_Client_ID")
				.append(" AND ((l.IsBillTo='Y' AND o.IsSOTrx='Y') OR (l.IsPayFrom='Y' AND o.IsSOTrx='N'))")
				.append(") ")
				.append("WHERE C_BPartner_ID IS NOT NULL AND BillTo_ID IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set BP BillTo from BP=" + no);
		//	Set Location from BPartner
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_BPartner_Location_ID=(SELECT MAX(C_BPartner_Location_ID) FROM C_BPartner_Location l")
				.append(" WHERE l.C_BPartner_ID=o.C_BPartner_ID AND o.AD_Client_ID=l.AD_Client_ID")
				.append(" AND ((l.IsShipTo='Y' AND o.IsSOTrx='Y') OR o.IsSOTrx='N')")
				.append(") ")
				.append("WHERE C_BPartner_ID IS NOT NULL AND C_BPartner_Location_ID IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set BP Location from BP=" + no);
		//
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No BP Location, ' ")
				.append("WHERE C_BPartner_ID IS NOT NULL AND (BillTo_ID IS NULL OR C_BPartner_Location_ID IS NULL)")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No BP Location=" + no);

		//	Set Country
		/**
		sql = new StringBuffer ("UPDATE I_Order o "
			  + "SET CountryCode=(SELECT MAX(CountryCode) FROM C_Country c WHERE c.IsDefault='Y'"
			  + " AND c.AD_Client_ID IN (0, o.AD_Client_ID)) "
			  + "WHERE C_BPartner_ID IS NULL AND CountryCode IS NULL AND C_Country_ID IS NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.fine("Set Country Default=" + no);
		 **/
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_Country_ID=(SELECT C_Country_ID FROM C_Country c")
				.append(" WHERE o.CountryCode=c.CountryCode AND c.AD_Client_ID IN (0, o.AD_Client_ID)) ")
				.append("WHERE C_BPartner_ID IS NULL AND C_Country_ID IS NULL AND CountryCode IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Country=" + no);
		//
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Country, ' ")
				.append("WHERE C_BPartner_ID IS NULL AND C_Country_ID IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Invalid Country=" + no);

		//	Set Region
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("Set RegionName=(SELECT MAX(Name) FROM C_Region r")
				.append(" WHERE r.IsDefault='Y' AND r.C_Country_ID=o.C_Country_ID")
				.append(" AND r.AD_Client_ID IN (0, o.AD_Client_ID)) ")
				.append("WHERE C_BPartner_ID IS NULL AND C_Region_ID IS NULL AND RegionName IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Region Default=" + no);
		//
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("Set C_Region_ID=(SELECT C_Region_ID FROM C_Region r")
				.append(" WHERE r.Name=o.RegionName AND r.C_Country_ID=o.C_Country_ID")
				.append(" AND r.AD_Client_ID IN (0, o.AD_Client_ID)) ")
				.append("WHERE C_BPartner_ID IS NULL AND C_Region_ID IS NULL AND RegionName IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Region=" + no);
		//
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Region, ' ")
				.append("WHERE C_BPartner_ID IS NULL AND C_Region_ID IS NULL ")
				.append(" AND EXISTS (SELECT * FROM C_Country c")
				.append(" WHERE c.C_Country_ID=o.C_Country_ID AND c.HasRegion='Y')")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Invalid Region=" + no);

		//	Product
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_Product_ID=(SELECT MAX(M_Product_ID) FROM M_Product p")
				.append(" WHERE o.ProductValue=p.Value AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE M_Product_ID IS NULL AND ProductValue IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Product from Value=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_Product_ID=(SELECT MAX(M_Product_ID) FROM M_Product p")
				.append(" WHERE o.UPC=p.UPC AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE M_Product_ID IS NULL AND UPC IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Product from UPC=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET M_Product_ID=(SELECT MAX(M_Product_ID) FROM M_Product p")
				.append(" WHERE o.SKU=p.SKU AND o.AD_Client_ID=p.AD_Client_ID) ")
				.append("WHERE M_Product_ID IS NULL AND SKU IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Product fom SKU=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Product, ' ")
				.append("WHERE M_Product_ID IS NULL AND (ProductValue IS NOT NULL OR UPC IS NOT NULL OR SKU IS NOT NULL)")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Invalid Product=" + no);

		//	Charge
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_Charge_ID=(SELECT C_Charge_ID FROM C_Charge c")
				.append(" WHERE o.ChargeName=c.Name AND o.AD_Client_ID=c.AD_Client_ID) ")
				.append("WHERE C_Charge_ID IS NULL AND ChargeName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Charge=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Charge, ' ")
				.append("WHERE C_Charge_ID IS NULL AND (ChargeName IS NOT NULL)")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Invalid Charge=" + no);
		//

		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Product and Charge, ' ")
				.append("WHERE M_Product_ID IS NOT NULL AND C_Charge_ID IS NOT NULL ")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Invalid Product and Charge exclusive=" + no);

		//	Tax
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET C_Tax_ID=(SELECT MAX(C_Tax_ID) FROM C_Tax t")
				.append(" WHERE o.TaxIndicator=t.TaxIndicator AND o.AD_Client_ID=t.AD_Client_ID) ")
				.append("WHERE C_Tax_ID IS NULL AND TaxIndicator IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (log.isLoggable(Level.FINE)) log.fine("Set Tax=" + no);
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Invalid Tax, ' ")
				.append("WHERE C_Tax_ID IS NULL AND TaxIndicator IS NOT NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("Invalid Tax=" + no);

		commitEx();

		//	-- New BPartner ---------------------------------------------------		
		//	Go through Order Records w/o C_BPartner_ID
		sql = new StringBuilder ("SELECT * FROM I_Order o ")
				.append("WHERE I_IsImported='N' AND C_BPartner_ID IS NULL").append (clientCheck);
		
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int Order_ID = 0;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			rs = pstmt.executeQuery ();
			while (rs.next ())
			{
				X_I_Order imp = new X_I_Order (getCtx (), rs, get_TrxName());
				if (imp.getBPartnerValue () == null)
				{
					if (imp.getEMail () != null)
						imp.setBPartnerValue (imp.getEMail ());
					else if (imp.getName () != null)
						imp.setBPartnerValue (imp.getName ());
					else
						continue;
				}
				if (imp.getName () == null)
				{
					if (imp.getContactName () != null)
						imp.setName (imp.getContactName ());
					else
						imp.setName (imp.getBPartnerValue ());
				}
				//	BPartner
				MBPartner bp = MBPartner.get (getCtx(), imp.getBPartnerValue(), get_TrxName());
				if (bp == null)
				{
					bp = new MBPartner (getCtx (), -1, get_TrxName());
					bp.setClientOrg (imp.getAD_Client_ID (), imp.getAD_Org_ID ());
					bp.setValue (imp.getBPartnerValue ());
					bp.setName (imp.getName ());
					if (!bp.save ())
						continue;
				}
				imp.setC_BPartner_ID (bp.getC_BPartner_ID ());

				//	BP Location
				MBPartnerLocation bpl = null; 
				MBPartnerLocation[] bpls = bp.getLocations(true);
				for (int i = 0; bpl == null && i < bpls.length; i++)
				{
					if (imp.getC_BPartner_Location_ID() == bpls[i].getC_BPartner_Location_ID())
						bpl = bpls[i];
					//	Same Location ID
					else if (imp.getC_Location_ID() == bpls[i].getC_Location_ID())
						bpl = bpls[i];
					//	Same Location Info
					else if (imp.getC_Location_ID() == 0)
					{
						MLocation loc = bpls[i].getLocation(false);
						if (loc.equals(imp.getC_Country_ID(), imp.getC_Region_ID(), 
								imp.getPostal(), "", imp.getCity(), 
								imp.getAddress1(), imp.getAddress2()))
							bpl = bpls[i];
					}
				}
				if (bpl == null)
				{
					//	New Location
					MLocation loc = new MLocation (getCtx (), 0, get_TrxName());
					loc.setAddress1 (imp.getAddress1 ());
					loc.setAddress2 (imp.getAddress2 ());
					loc.setCity (imp.getCity ());
					loc.setPostal (imp.getPostal ());
					if (imp.getC_Region_ID () != 0)
						loc.setC_Region_ID (imp.getC_Region_ID ());
					loc.setC_Country_ID (imp.getC_Country_ID ());
					if (!loc.save ())
						continue;
					//
					bpl = new MBPartnerLocation (bp);
					bpl.setC_Location_ID (loc.getC_Location_ID ());
					if (!bpl.save ())
						continue;
				}
				imp.setC_Location_ID (bpl.getC_Location_ID ());
				imp.setBillTo_ID (bpl.getC_BPartner_Location_ID ());
				imp.setC_BPartner_Location_ID (bpl.getC_BPartner_Location_ID ());

				//	User/Contact
				if (imp.getContactName () != null 
						|| imp.getEMail () != null 
						|| imp.getPhone () != null)
				{
					MUser[] users = bp.getContacts(true);
					MUser user = null;
					for (int i = 0; user == null && i < users.length;  i++)
					{
						String name = users[i].getName();
						if (name.equals(imp.getContactName()) 
								|| name.equals(imp.getName()))
						{
							user = users[i];
							imp.setAD_User_ID (user.getAD_User_ID ());
						}
					}
					if (user == null)
					{
						user = new MUser (bp);
						if (imp.getContactName () == null)
							user.setName (imp.getName ());
						else
							user.setName (imp.getContactName ());
						user.setEMail (imp.getEMail ());
						user.setPhone (imp.getPhone ());
						if (user.save ())
							imp.setAD_User_ID (user.getAD_User_ID ());
					}
				}
				imp.saveEx();
			}	//	for all new BPartners
			//
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, "BP - " + sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No BPartner, ' ")
				.append("WHERE C_BPartner_ID IS NULL")
				.append(" AND I_IsImported<>'Y'").append (clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No BPartner=" + no);

		commitEx();

		//	-- New Orders -----------------------------------------------------	
		int noInsert = 0;
		int noInsertLine = 0;

		//	Go through Order Records w/o
		/*sql = new StringBuilder ("SELECT I_Order.* FROM I_Order INNER JOIN (SELECT DocumentNo,C_DocType_ID,COUNT(*) as numberoflines ")
				.append(" FROM I_Order GROUP BY DocumentNo,C_DocType_ID) Temp_IO ON Temp_IO.DocumentNo=I_Order.DocumentNo AND Temp_IO.C_DocType_ID=I_Order.C_DocType_ID")
			  .append(" WHERE I_Order.I_IsImported='N' AND I_Order.DocumentNo||I_Order.C_DocType_ID NOT IN (SELECT DocumentNo||C_DocType_ID FROM C_Order WHERE DocStatus = 'CO') " )
			 .append(" AND I_Order.opos_numberoflines::int=Temp_IO.numberoflines").append (clientCheck);*/

		//sql.append(" ORDER BY I_Order.C_BPartner_ID,I_Order.c_doctype_id,I_Order.documentno,I_Order.fiscalnumber,I_Order.I_Order_ID");		

		sql = new StringBuilder ("SELECT * FROM I_Order o ")
				.append("WHERE I_IsImported='N' AND NOT EXISTS (SELECT 1 FROM C_ORDER "
						+ "WHERE DOCUMENTNO = o.DOCUMENTNO AND C_DocTypeTarget_ID = o.C_DOCTYPE_ID AND DOCSTATUS!='VO')").append (clientCheck);
		
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null){
			sql.append(docnofilter);
			
			sql.append(" AND o.DocumentNo NOT IN (SELECT DocumentNo from adempiere.I_Order WHERE I_IsImported='E'").append(clientCheck).append(") ");
		}
		sql.append(" ORDER BY DateOrdered,Documentno,OPOS_line,C_BPartner_ID, BillTo_ID, C_BPartner_Location_ID, I_Order_ID");

		MOrder order = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			rs = pstmt.executeQuery ();
			//
			int oldC_BPartner_ID = 0;
			int oldBillTo_ID = 0;
			int oldC_BPartner_Location_ID = 0;
			String oldDocumentNo = "";
			String oldfiscalnumber = "";

			//

			int lineNo = 0;
			int OPOS_line = 0;
			int OPOS_numberoflines =0;
			while (rs.next ())
			{

				X_I_Order imp = new X_I_Order (getCtx (), rs, get_TrxName());
				

				String cmpDocumentNo = imp.getDocumentNo();
				String cmpfiscalnumber = imp.get_ValueAsString("fiscalNumber");
				if (cmpDocumentNo == null)
					cmpDocumentNo = "";
				if (cmpfiscalnumber == null)
					cmpfiscalnumber = "";

				//	New Order
				if (oldC_BPartner_ID != imp.getC_BPartner_ID() 
						//|| oldC_BPartner_Location_ID != imp.getC_BPartner_Location_ID()
						//|| oldBillTo_ID != imp.getBillTo_ID()
						|| !oldfiscalnumber.equals(cmpfiscalnumber)
						|| !oldDocumentNo.equals(cmpDocumentNo))
				{
					if (order != null)
					{
						if (OPOS_numberoflines!= noInsertLine){
							//throw new IllegalStateException("Order Process Failed: " + order + " - incomplete number of lines");
							log.warning("Order Process Failed: " + order + " - incomplete number of lines");
						
						}
						if (m_docAction != null && m_docAction.length() > 0)
						{
							order.setDocAction(m_docAction);
							if(!order.processIt (m_docAction)) {
								log.warning("Order Process Failed: " + order + " - " + order.getProcessMsg());
								throw new IllegalStateException("Order Process Failed: " + order + " - " + order.getProcessMsg());

							}
						}
						order.saveEx();
						System.out.println(nofiscal);
						if(m_docAction.equals(null) && m_docAction.compareTo(MOrder.DOCACTION_Complete)==0){
							MInvoice invoice = new MInvoice(getCtx(),order.getC_Invoice_ID(),get_TrxName());
							if(invoice != null)
								if(imp.get_ValueAsString("fiscalNumber") != null)
									invoice.set_ValueOfColumn("fiscalNumber", nofiscal);
							if(imp.get_ValueAsString("FiscalPrintSerial") != null)
								invoice.set_ValueOfColumn("FiscalPrintSerial", noserial);
							invoice.saveEx();
						}	
					}
					System.out.println("Procesando Orden Nro: "+imp.getDocumentNo());
					oldC_BPartner_ID = imp.getC_BPartner_ID();
					oldC_BPartner_Location_ID = imp.getC_BPartner_Location_ID();
					oldBillTo_ID = imp.getBillTo_ID();
					oldDocumentNo = imp.getDocumentNo();
					oldfiscalnumber = imp.get_ValueAsString("fiscalNumber");
					if (oldDocumentNo == null)
						oldDocumentNo = "";
					if (oldfiscalnumber == null)
						oldfiscalnumber = "";					
					//
					order = new MOrder (getCtx(), 0, get_TrxName());
					order.setClientOrg (imp.getAD_Client_ID(), imp.getAD_Org_ID());
					order.setC_DocTypeTarget_ID(imp.getC_DocType_ID());
					order.setIsSOTrx(imp.isSOTrx());
					if (imp.getDeliveryRule() != null ) {
						order.setDeliveryRule(imp.getDeliveryRule());
					}
					/*if (imp.get_Value("PaymentRule") != null ) {
						order.set_CustomColumn("PaymentRule", imp.get_Value("PaymentRule"));
					}
					else order.setPaymentRule("B");*/
					order.setPaymentRule("B");

					if (imp.getDocumentNo() != null)
						order.setDocumentNo(imp.getDocumentNo());
					//	Ship Partner
					order.setC_BPartner_ID(imp.getC_BPartner_ID());
					order.setC_BPartner_Location_ID(imp.getC_BPartner_Location_ID());
					if (imp.getAD_User_ID() != 0)
						order.setAD_User_ID(imp.getAD_User_ID());
					//	Bill Partner
					order.setBill_BPartner_ID(imp.getC_BPartner_ID());
					order.setBill_Location_ID(imp.getBillTo_ID());
					//
					if (imp.getDescription() != null)
						order.setDescription(imp.getDescription());
					order.setC_PaymentTerm_ID(imp.getC_PaymentTerm_ID());
					order.setM_PriceList_ID(imp.getM_PriceList_ID());
					order.setM_Warehouse_ID(imp.getM_Warehouse_ID());
					if (imp.getM_Shipper_ID() != 0)
						order.setM_Shipper_ID(imp.getM_Shipper_ID());
					//	SalesRep from Import or the person running the import
					if (imp.getSalesRep_ID() != 0)
						order.setSalesRep_ID(imp.getSalesRep_ID());
					if (order.getSalesRep_ID() == 0)
						order.setSalesRep_ID(getAD_User_ID());
					//
					if (imp.getAD_OrgTrx_ID() != 0)
						order.setAD_OrgTrx_ID(imp.getAD_OrgTrx_ID());
					if (imp.getC_Activity_ID() != 0)
						order.setC_Activity_ID(imp.getC_Activity_ID());
					if (imp.getC_Campaign_ID() != 0)
						order.setC_Campaign_ID(imp.getC_Campaign_ID());
					if (imp.getC_Project_ID() != 0)
						order.setC_Project_ID(imp.getC_Project_ID());
					//
					if (imp.getDateOrdered() != null)
						order.setDateOrdered(imp.getDateOrdered());
					if (imp.getDateAcct() != null)
						order.setDateAcct(imp.getDateAcct());

					// Set Order Source
					if (imp.getC_OrderSource() != null)
						order.setC_OrderSource_ID(imp.getC_OrderSource_ID());
					//
					order.saveEx();
					noInsert++;
					lineNo = 10;
					OPOS_line=0;
					OPOS_numberoflines = imp.get_ValueAsInt("OPOS_numberoflines");
				}
				//if(OPOS_line==imp.get_ValueAsInt("OPOS_line")) {
					imp.setC_Order_ID(order.getC_Order_ID());
				
					OPOS_line++;
					//	New OrderLine
					MOrderLine line = new MOrderLine (order);
					line.setLine(lineNo);
					lineNo += 10;
					if (imp.getM_Product_ID() != 0)
						line.setM_Product_ID(imp.getM_Product_ID(), true);
					if (imp.getC_Charge_ID() != 0)
						line.setC_Charge_ID(imp.getC_Charge_ID());
					line.setQty(imp.getQtyOrdered());
					line.setPrice();
					if (imp.getPriceActual().compareTo(Env.ZERO) != 0)
						line.setPrice(imp.getPriceActual());
					if (imp.getC_Tax_ID() != 0)
						line.setC_Tax_ID(imp.getC_Tax_ID());
					else
					{
						line.setTax();
						imp.setC_Tax_ID(line.getC_Tax_ID());
					}
					if (imp.getFreightAmt() != null)
						line.setFreightAmt(imp.getFreightAmt());
					if (imp.getLineDescription() != null)
						line.setDescription(imp.getLineDescription());
					line.saveEx();
					imp.setC_OrderLine_ID(line.getC_OrderLine_ID());
					imp.setI_IsImported(true);
					imp.setProcessed(true);
					nofiscal = imp.get_ValueAsString("fiscalNumber");
					noserial = imp.get_ValueAsString("FiscalPrintSerial");
					//
					if (imp.save())
						noInsertLine++;
				//}else {
					//sql = new StringBuilder ("UPDATE I_Order  ")
					//		.append("SET documentno='ANU-'||'"+imp.getDocumentNo()+"'")
					//		.append(" WHERE I_Order_ID="+imp.get_ID());
					
					//no = DB.executeUpdate(sql.toString(), get_TrxName());
					
				//}
			}
			if (order != null)
			{
				if (OPOS_numberoflines!= noInsertLine) {
					//throw new IllegalStateException("Order Process Failed: " + order + " - incomplete number of lines");
					log.warning("Order Process Failed: " + order + " - incomplete number of lines");
				}
				if (m_docAction.length() > 0)
				{
					order.setDocAction(m_docAction);
					if(!order.processIt (m_docAction)) {
						log.warning("Order Process Failed: " + order + " - " + order.getProcessMsg());
						throw new IllegalStateException("Order Process Failed: " + order + " - " + order.getProcessMsg());

					}
				}
				order.saveEx();
				System.out.println(nofiscal);
				
				if(m_docAction.compareTo(MOrder.DOCACTION_Complete)==0){
					MInvoice invoice = new MInvoice(getCtx(),order.getC_Invoice_ID(),get_TrxName());
					if(invoice != null)
						if(nofiscal != null)
							invoice.set_ValueOfColumn("fiscalNumber", nofiscal);
					if(noserial != null)
						invoice.set_ValueOfColumn("FiscalPrintSerial", noserial);
					invoice.saveEx();
				}
			}				
		}
		catch (Exception e)
		{			
			log.log(Level.SEVERE, "Order - " + sql.toString(), e);
			noInsert = 0;
			noInsertLine = 0;
			rollback();
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		//	Set Error to indicator to not imported
		sql = new StringBuilder ("UPDATE I_Order o ")
				.append("SET I_IsImported='N', Updated=now() ")
				.append("WHERE I_IsImported<>'Y'").append(clientCheck);
		if(m_AD_Org_ID!=0)
			sql.append(orgfilter);
		
		if(m_DocumentNo!= null) {
			
			sql.append(docnofilter);
		}
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		addLog (0, null, new BigDecimal (no), "@Errors@");
		//
		addLog (0, null, new BigDecimal (noInsert), "@C_Order_ID@: @Inserted@");
		addLog (0, null, new BigDecimal (noInsertLine), "@C_OrderLine_ID@: @Inserted@");
		StringBuilder msgreturn = new StringBuilder("#").append(noInsert).append("/").append(noInsertLine);
		if(m_DocumentNo!= null) {
			if(order !=null)
				return String.valueOf(order.get_ID());

			return null;
		}

		return msgreturn.toString();
	}	//	doIt

}	//	ImportOrder
