package com.cds.posintegration.component;

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.compiere.model.MInOut;
import org.compiere.model.MInventory;
import org.compiere.model.MMovement;
import org.compiere.model.MOrder;
import org.compiere.model.MProductPrice;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.osgi.service.event.Event;

public class POSEventHandler extends AbstractEventHandler{
	private static CLogger log = CLogger.getCLogger(POSEventHandler.class);
	@Override
	protected void doHandleEvent(Event event) {
		/*PO po = getPO(event);
		String type = event.getTopic();
		log.info(po.get_TableName() + " Type: " + type);
		StringBuilder sql = new StringBuilder("UPDATE M_Product SET Updated = now() WHERE ");;
		String whereclause = "";
		int no = 0;
		if(type.equals(IEventTopics.DOC_AFTER_COMPLETE)){

			if (po.get_TableName().equals(MOrder.Table_Name) ) {
				whereclause = "M_Product_ID IN (SELECT M_Product_ID FROM C_Order WHERE C_Order_ID = "
						+po.get_ID()+")";
			}else if (po.get_TableName().equals(MMovement.Table_Name) ) {
				whereclause = "M_Product_ID IN (SELECT M_Product_ID FROM M_Movement WHERE M_Movement_ID = "
						+po.get_ID()+")";
			}else if (po.get_TableName().equals(MInOut.Table_Name) ) {
				whereclause = "M_Product_ID IN (SELECT M_Product_ID FROM M_InOut WHERE M_InOut_ID = "
						+po.get_ID()+")";
			}else if (po.get_TableName().equals(MInventory.Table_Name) ) {
				whereclause = "M_Product_ID IN (SELECT M_Product_ID FROM M_Inventory WHERE M_Inventory_ID = "
						+po.get_ID()+")";
			}
		}
		if(type.equals(IEventTopics.PO_AFTER_CHANGE)){
			if (po.get_TableName().equals(MProductPrice.Table_Name) ) {
				whereclause = "M_Product_ID IN (SELECT M_Product_ID FROM M_ProductPrice WHERE M_ProductPrice_ID = "
						+po.get_ID()+")";
			}
		}
		if(type.equals(IEventTopics.PO_AFTER_NEW)){
			if (po.get_TableName().equals(MProductPrice.Table_Name) ) {
				whereclause = "M_Product_ID IN (SELECT M_Product_ID FROM M_ProductPrice WHERE M_ProductPrice_ID = "
						+po.get_ID()+")";
			}
		}
		if(whereclause.compareTo("")!=0) {
			sql.append(whereclause);
			no = DB.executeUpdate(sql.toString(),null);
		}
*/		
	}

	@Override
	protected void initialize() {
	/*	registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE, MOrder.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE, MMovement.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE, MInOut.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE, MInventory.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MProductPrice.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_NEW, MProductPrice.Table_Name);
		*/
	}

}
