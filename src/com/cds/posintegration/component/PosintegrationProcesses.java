package com.cds.posintegration.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

import com.cds.posintegration.process.ImportInventoryMove;
import com.cds.posintegration.process.ImportOrder;


public class PosintegrationProcesses implements IProcessFactory{

	@Override
	public ProcessCall newProcessInstance(String className) {
		// TODO Auto-generated method stub
		if(className.equals("com.cds.posintegration.process.ImportInventoryMove"))
			return new ImportInventoryMove();
		if(className.equals("com.cds.posintegration.process.ImportOrder"))
			return new ImportOrder();
		return null;
	}

}
